# Eduardo Pacheco Celeste #

-----------------------
**E-mail:** eduardopc91@hotmail.com		
**Mobile:** 55(11)98982-8540	
**Medium:** https://medium.com/@pacheco.eduardo			
**Linkedin:** https://www.linkedin.com/in/eduardopachecoceleste/	
**Site:** Building	
**Location:** São Paulo/Brazil	

Quality engineer with focus in Automated tests, enthusiastic for the new technologies and a lover by knowledge

## Education
-----------------------
	
Bachelor in System Analyst at Universidade Bandeirante de São Paulo (2013)	

## Professional Experience
-----------------------
	
##### **Quality Analyst at [ PayU ](https://www.payulatam.com/br/) (Current job - Since 09/2015)**

At PayU the quality team is responsible to ensure the internal products quality making automated and manual tests and these automated tests are executed in our continuous integration server, providing feedback faster after each change. Also, in there I had my first contact with Scrum methodology.

Stack: Java, Selenium WebDriver, MySQL, PostgreSQL, NodeJS, Javascript, Protractor, MongoDB, Redis  

##### **Software Test Automation at [ InMetrics ](http://www.inmetrics.com.br/) (06/2015 até 09/2015)**

During the time that I stayed at InMetrics I worked at Globo project. In this project I met SilkTest tool from Borland and the HP ALM to make the tests management. I was also responsible to make this both tools from the competing companies to talk with each other using REST.

Stack: Java, SilkTest, VBA, Jmeter

##### **Test Analyst at [ Iteris ](http://www.iteris.com.br/pt-br/index.html) (09/2013 até 06/2015)**

In this job I had my first contact with manual testing and open source tools like Selenium and JMeter but I worked with others comercial tools too (Microsoft CodedUI). In there I participated a lot of projects like Centauro, Rede, Crefisa, Serasa among others. But my most challenging work in there was a development mobile/web project using Java and DotNet respectively. Also, I had my first contact with agile methodologies, such as Kanban.

Stack: Selenium Webdriver, DotNet, Java, JMeter, Micrososft CodedUI

##### **Test Analyst at [ UOLDiveo ](http://uoldiveo.com.br/#rmcl) (08/2012 até 09/2013)**
At UOLDiveo I had my first contact with software test automation. In there I participated two projects, in the first one (Bradesco) I automated scenarios using HP QuickTest tool and HP ALM to make the tests management. Moreover I need to teach the tests importance to the Bradesco staff and create reports to show our job using macros in the Excel. After this project, I was resposible to create automated scenarios to B2W company (Americanas, Shoptime and Submarino). The last one was my principal project because I was the only one employee making this job there.

Stack: HP QuickTest, HP ALM, HP Sprinter, VBScript

## Certifications
-----------------------

CTFL - Certified Tester Foundation Level